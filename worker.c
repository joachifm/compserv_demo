#include "common.h"

int main(void) {
    while (1) {
        LOG_INFO("worker awaits signal to begin working ...");
        char c;
        if (read(0, &c, 1) == -1) {
            perror("read");
            exit(-errno);
        }

        struct data_handle in = open_dataset("floater1_in", float);
        struct data_handle out = open_dataset("floater1_out", float);

        // Pointless busywork ahead; details irrelevant
        LOG_INFO("worker generates heat ...");
        float* const indata = in.addr;
        float* const outdata = out.addr;
        float sum = 0.0;
        size_t i = 0;
        for (; i < in.len; ++i) {
            sum += indata[i];
        }
        *outdata = sum / (float) i;
        fprintf(stderr, "worker produced result %f\n", *outdata);
        usleep(2500000); // sleep to make controller think we're working

        LOG_INFO("worker produces done signal ...");
        if (write(1, &c, 1) == -1) {
            perror("worker write is_done");
            exit(-errno);
        }
        close_dataset(&in);
        close_dataset(&out);
    }
}
