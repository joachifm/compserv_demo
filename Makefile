.SUFFIXES: .c .o

SHELL ?= /bin/sh
CC ?= cc
CFLAGS = -ggdb3 -g3

LDLIBS = -lrt

.PHONY: all
all: worker controller

worker: worker.c
	$(CC) $(LDLIBS) -o $@ $<

controller: controller.c
	$(CC) $(LDLIBS) -o $@ $<
