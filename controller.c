#include "common.h"
#include <sys/wait.h>

int main(int argc, char* argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: controller data_len\n");
        exit(2);
    }
    int optval_datalen = atoi(argv[1]); // no user would ever pass non-numeric input

    // Communication between controller and worker happens over unnamed pipes aka.
    // channels.  Channels are one-way so we need one for sending commands and one
    // for receiving the response.
    //
    // Awaiting a command/response is an ordinary blocking read on the
    // read end of a channel pipe; sending a message is an ordinary write to
    // the write end of a channel pipe.
    //
    // The controller-worker protocol may be arbitrarily complex.  In this demo, we only
    // send single-byte messages to signal "begin work"/"work is done".
    int chan_req[2]; // command channel, controller -> worker
    if (pipe(chan_req) == -1) {
        perror("pipe");
        exit(-errno);
    }
    int chan_res[2]; // response channel, worker -> controller
    if (pipe(chan_res) == -1) {
        perror("pipe");
        exit(-errno);
    }

    // Spawn a single worker to handle all our requests.  In a realistic implementation
    // the worker is probably an executor/scheduler that delegates jobs to subordinate
    // processes of its own.
    pid_t childpid = fork();
    if (childpid == 0) { // execution continues in child
        // Attach the read end of the request channel pipe to worker's stdin
        close(chan_req[1]);
        dup2(chan_req[0], 0);

        // Attach the write end of the response channel pipe to worker's stdout
        close(chan_res[0]);
        dup2(chan_res[1], 1);

        // Replace process image; inheriting file descriptors
        //
        // TODO(joachifm) because fds are inherited probably more robust to attach
        // channels to non-standard fds; as-is we need to be careful to not write to
        // stdout within the worker lest we signal "is done" prematurely ...
        execl("/usr/bin/python", "python", "./worker.py", (char *)0);
        //execl("./worker", "worker", (char *)0);
    } else if (childpid == -1) {
        perror("fork");
        exit(-errno);
    } else { // execution continues in parent
        close(chan_req[0]); // only writes to request channel
        close(chan_res[1]); // only reads from response channel

        size_t const datalen = optval_datalen;
        struct data_handle const in = create_dataset("floater1_in", datalen, float);
        struct data_handle const out = create_dataset("floater1_out", datalen, float);
        float* const indata = in.addr;
        float* const outdata = out.addr;

        while (1) {
            LOG_INFO("master is filling work area with data");

            for (size_t i = 0; i < in.len; ++i) {
                indata[i] = 3.14f;
            }

            LOG_INFO("master signals worker to begin");
            char flag;
            if (write(chan_req[1], &flag, 1) == -1) {
                perror("master write");
                exit(-errno);
            }

            LOG_INFO("master is waiting for worker to finish");
            if (read(chan_res[0], &flag, 1) == -1) {
                perror("master read");
                exit(-errno);
            }

            fprintf(stderr, "master received result: %f\n", *outdata);
        }

        kill(childpid, SIGTERM);
        waitpid(-1, 0, WNOHANG);
        exit(0);
    }
}
