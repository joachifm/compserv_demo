#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define LOG_INFO(MESSAGE) fprintf(stderr, "%s\n", MESSAGE)

/**
 * A shared memory segment work area.
 */
struct data_handle {
    char name[8]; /* short name, including trailing NUL */

    void* addr; /* memory area address */
    size_t addr_len; /* memory area size */

    size_t len; /* number of data items */
    size_t dtype; /* data item type identified by the size of the underlying storage */
};

/**
 * Create new dataset in a shared memory segment.
 *
 * @param name  dataset name
 * @param n     number of data items
 * @param dtype data item size
 *
 */
static struct data_handle _create_dataset(char const* name, size_t n, size_t dtype) {
    // TODO(joachifm) size_t * size_t is unsafe
    size_t addr_len = n * dtype;

    int fd = shm_open(name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("create_dataset: shm_open");
        exit(-errno);
    }
    /* TODO(joachifm) ftruncate SIGBUS if shm is full ... */
    if (ftruncate(fd, addr_len) == -1) {
        perror("create_dataset: ftruncate");
        exit(-errno);
    }
    void* addr = mmap(NULL, addr_len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        perror("create_dataset: mmap");
        exit(-errno);
    }
    close(fd);

    struct data_handle hdl = { .addr = addr, .addr_len = addr_len, .len = n, .dtype = dtype };
    strncpy(hdl.name, name, 7);
    hdl.name[7] = '\0';
    return hdl;
}

static struct data_handle _open_dataset(char const* name, size_t dtype) {
    int fd = shm_open(name, O_RDWR, S_IRUSR | S_IWUSR);
    if (fd == -1) {
        perror("open_dataset: shm_open");
        exit(-errno);
    }

    struct stat sb = {0};
    if (fstat(fd, &sb) == -1) {
        perror("open_dataset: stat");
        exit(-errno);
    }
    size_t addr_len = sb.st_size;

    void* addr = mmap(NULL, addr_len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        perror("open_dataset: mmap");
        exit(-errno);
    }
    close(fd);

    size_t len = addr_len / dtype;
    struct data_handle hdl = { .addr = addr, .addr_len = addr_len, .len = len, .dtype = dtype };
    strncpy(hdl.name, name, 7);
    hdl.name[7] = '\0';
    return hdl;
}

static void close_dataset(struct data_handle* hdl) {
    munmap(hdl->addr, hdl->addr_len);
    *hdl = (struct data_handle){0};
}

// Sugary wrappers that allow symbolic dtype (as in float or int)
#define open_dataset(NAME, DTYPE) _open_dataset(NAME, sizeof(DTYPE))
#define create_dataset(NAME, N, DTYPE) _create_dataset(NAME, N, sizeof(DTYPE))

// Convenience ...
#define with_existing_dataset(NAME, DTYPE, VAR)                         \
    for (struct data_handle (VAR) = open_dataset(NAME, DTYPE); (VAR).addr; close_dataset(&(VAR)))

#define with_created_dataset(NAME, LEN, DTYPE, VAR)                      \
    for (struct data_handle (VAR) = create_dataset(NAME, LEN, DTYPE); (VAR).addr; close_dataset(&(VAR)))
