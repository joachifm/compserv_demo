if __name__ == '__main__':
    from multiprocessing.shared_memory import SharedMemory
    import numpy as np
    import sys

    while True:
        print('worker awaiting command', file=sys.stderr)
        c = sys.stdin.read(1)

        print('worker processing indata', file=sys.stderr)
        indata_mem = SharedMemory(name='floater1_in')
        indata_buf = indata_mem.buf
        indata_len = int(indata_mem.size / 4)
        indata_arr = np.ndarray((indata_len,), buffer=indata_buf, dtype=np.float32)
        print(f'worker indata set: {indata_arr.shape}', file=sys.stderr)
        result = indata_arr.mean()
        print(f'worker result {result}', file=sys.stderr)

        outdata_mem = SharedMemory(name='floater1_out')
        outdata_buf = outdata_mem.buf
        outdata_arr = np.ndarray((1,), dtype=np.float32, buffer=outdata_buf)
        outdata_arr[0] = result

        print('worker signalling job completion', file=sys.stderr)
        sys.stdout.write('done'); sys.stdout.flush()
